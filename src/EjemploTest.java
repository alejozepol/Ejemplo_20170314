
import junit.framework.Test;
import junit.framework.TestCase;
import static junit.framework.TestCase.assertTrue;
import junit.framework.TestSuite;

/**
 *
 * @author AlejoZepol
 */
public class EjemploTest extends TestCase {

    
 
public void testConcat() {
        String s = "hola";
        String s2 = s.concat(" que tal");
        assertTrue(s2.equals("hola que"));
    } // testConcat

    public static Test suite() {
        return new TestSuite(EjemploTest.class);
    } // suite

    public static void main(String[] args) {
        junit.textui.TestRunner.run(suite());
    }   
}
